<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
// safe redirect
function smart_quiz_redirect($url) {
    echo "<meta http-equiv='refresh' content='0;url=$url' />";
    exit;
}
