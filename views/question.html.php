<div class="wrap">
    <h1><?php printf(__('Добавить/Редактировать вопросы в "%s"', 'smart_quiz'), $quiz->title) ?></h1>

    <div class="postbox-container squiz-width-100">

        <p><a href="admin.php?page=smart_quizzes"><?php _e('Назад к квизу', 'smart_quiz') ?></a>
            |
            <a href="admin.php?page=smart_quiz_questions&quiz_id=<?php echo $quiz->id ?>"><?php _e('Назад к вопросам', 'smart_quiz') ?></a>
        </p>

        <div id="squiz-choices">

            <app-question question_id="<?php echo ($question->id > 0) ? $question->id : 0 ?>"
                          quiz_id="<?php echo $quiz->id ?>"

            ></app-question>
        </div>

    </div>