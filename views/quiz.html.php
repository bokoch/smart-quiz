<div class="wrap">
	<h1><?php _e('Добавить/Редактровать квиз', 'smart_quiz')?></h1>
	
	<div class="postbox-container squiz-width-100">
	
		<p><a href="admin.php?page=smart_quizzes"><?php _e('Назад к квизам', 'smart_quiz')?></a>
		<?php if(!empty($quiz->id)):?>
			| <a href="admin.php?page=smart_quiz_questions&quiz_id=<?php echo $quiz->id?>"><?php _e('Открыть вопросы', 'smart_quiz')?></a>
		<?php endif;?></p>
		
		<form method="post" onsubmit="return validateSmartQuiz(this);">
			<div class="squiz-input-wrapper"><label><?php _e('Заголовок', 'smart_quiz')?></label>
                <input type="text" name="title" size="60" value="<?php echo stripslashes(@$quiz->title)?>" >
            </div>

            <div class="squiz-input-wrapper"><label><?php _e('Admin Email', 'smart_quiz')?></label>
                <input type="text" name="admin_email" size="60" value="<?php echo stripslashes(@$quiz->admin_email)?>">
            </div>

			<div><input type="submit" name="submit" value="<?php _e('Сохранить квиз', 'smart_quiz')?>" class="button-primary"></div>
			<?php wp_nonce_field('smart_quiz');?>
		</form>
		
	</div>

</div>

<script type="text/javascript" >
function validateSmartQuiz(frm) {
    let returnFlag = true;

	if(frm.title.value == '') {
	    const msg = '<p class="squiz-error"><?php _e('Заголовок обязателен', 'smart_quiz')?></p>'
        frm.title.parentElement.insertAdjacentHTML('beforeend', msg);
        returnFlag = false;
	}

    if(frm.admin_email.value == '') {
        const msg = '<p class="squiz-error"><?php _e('Email обязателен', 'smart_quiz')?></p>'
        frm.admin_email.parentElement.insertAdjacentHTML('beforeend', msg);
        returnFlag = false;
    }
	
	return returnFlag;
}
</script>