<div class="wrap">
    <h1><?php _e('Smart Quizzes', 'smart_quiz') ?></h1>

    <div class="postbox-container squiz-width-100">

        <p><a href="admin.php?page=smart_quizzes&action=add"><?php _e('Добавить новый квиз', 'smart_quiz') ?></a></p>

        <?php if (sizeof($quizzes)): ?>
            <table class="widefat">
                <tr>
                    <th><?php _e('Заголовок', 'smart_quiz') ?></th>
                    <th><?php _e('Admin Email', 'smart_quiz') ?></th>
                    <th><?php _e('Вопросы', 'smart_quiz') ?></th>
                    <th><?php _e('Редактировать/Удалить', 'smart_quiz') ?></th>
                </tr>
                <?php foreach ($quizzes as $quiz):
                    $class = ('alternate' == @$class) ? '' : 'alternate'; ?>
                    <tr class="<?php echo $class ?>">
                        <td><?php if (!empty($quiz->post)) echo "<a href='" . get_permalink($quiz->post->ID) . "' target='_blank'>";
                            echo stripslashes($quiz->title);
                            if (!empty($quiz->post)) echo "</a>"; ?>
                        </td>
                        <td><?php if (!empty($quiz->post)) echo "<a href='" . get_permalink($quiz->post->ID) . "' target='_blank'>";
                            echo stripslashes($quiz->admin_email);
                            if (!empty($quiz->post)) echo "</a>"; ?>
                        </td>
                        <td>
                            <a href="admin.php?page=smart_quiz_questions&quiz_id=<?php echo $quiz->id ?>">
                                <?php _e('Открыть вопросы', 'smart_quiz') ?>
                            </a>
                        </td>

                        <td>
                            <a href="admin.php?page=smart_quizzes&action=edit&id=<?php echo $quiz->id ?>"><?php _e('Редактировать', 'smart_quiz') ?></a>
                            | <a href="#"
                                 onclick="confirmDelQuiz(<?php echo $quiz->id ?>);return false;"><?php _e('Удалить', 'smart_quiz') ?></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        <?php else: ?>
            <p><?php _e('Еще нету ниодного квиза.', 'smart_quiz') ?></p>
        <?php endif; ?>

    </div>

</div>

<script type="text/javascript">
    function confirmDelQuiz(id) {

        swal({
            title: '<?php _e('Вы уверены?', 'smart_quiz')?>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '<?php _e('Да, удалить!', 'smart_quiz')?>'
        }).then((result) => {
            if (result.value) {
                window.location = 'admin.php?page=smart_quizzes&del=1&id=' + id;
            }
        })
    }

</script>