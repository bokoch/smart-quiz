<div class="wrap">
	<h1><?php printf(__('Управлять вопросами в %s', 'smart_quiz'), $quiz->title);?> </h1>
	<div class="postbox-container squiz-width-100" >
	
		<p><a href="admin.php?page=smart_quizzes"><?php _e('Назад к квизам', 'smart_quiz')?></a>
			| <a href="admin.php?page=smart_quizzes&action=edit&id=<?php echo $quiz->id?>"><?php _e('Редактировать квиз', 'smart_quiz')?></a>
		</p>
		
		<p><a href="admin.php?page=smart_quiz_questions&action=add&quiz_id=<?php echo $quiz->id?>"><?php _e('Добавить новый вопрос', 'smart_quiz')?></a></p>
		<?php if(sizeof($questions)):?>
            <div id="squiz-questions">
                <list-question quiz_id="<?php echo $quiz->id ?>"></list-question>
            </div>
			
		<?php endif;?>
	
	</div>
</div>