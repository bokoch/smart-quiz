<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
// handle all ajax
function smartquiz_ajax() {
    $action = empty($_POST['smartquiz_action']) ? 'answer' : $_POST['smartquiz_action'];
	
	switch($action) {
        case 'answer':
            echo SmartQuizQuizzes :: answer_question($_POST);
            break;
        case 'get_quiz_front':
            echo SmartQuizQuizzes :: get_quiz_front($_POST['quiz_id']);
            break;
		case 'question_type':
		    echo SmartQuizQuestions::list_question_types();
		    break;
		case 'admin_add_question':
		    echo SmartQuizQuestions::ajax_add_question($_POST);
		    break;
		case 'get_question':
		    echo SmartQuizQuestions::ajax_get_question($_POST['question_id']);
		    break;
		case 'get_prev_question':
		    echo SmartQuizQuestions::ajax_get_prev_question($_POST['question_id']);
		    break;
		case 'get_question_conditions':
		    echo SmartQuizQuestions::ajax_get_question_conditions($_POST['question_id']);
		    break;
		case 'get_question_list':
		    echo SmartQuizQuestions::ajax_get_question_list($_POST['quiz_id']);
		    break;
		case 'update_question_sort_order':
		    echo SmartQuizQuestions::ajax_update_question_sort_order($_POST);
		    break;
		default:
			echo SmartQuizQuizzes :: answer_question($_POST);
		break;
	}

	exit;
}