<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class SmartQuizQuestions {
	static function manage() {
 		$action = empty($_GET['action']) ? 'list' : $_GET['action']; 
		switch($action) {
			case 'add':
				self :: add_question();
			break;
			case 'edit': 
				self :: edit_question();
			break;
			case 'list':
			default:
				self :: list_questions();	 
			break;
		}
	}
	
	static function add_question() {
        global $wpdb;
        $_question = new SmartQuizQuestion();
        $_quiz = new SmartQuizQuiz();
        if(!empty($_POST['submit']) and check_admin_referer('smart_quiz_question')) {
            try {
                $_POST['quiz_id'] = intval($_GET['quiz_id']);
                $qid = $_question->add($_POST);
                chained_redirect("admin.php?page=smart_quiz_questions&quiz_id=".intval($_GET['quiz_id']));
            }
            catch(Exception $e) {
                $error = $e->getMessage();
            }
        }

        $quiz = $_quiz->getQuiz($_GET['quiz_id']);

        $question_types = $_question->getQuestionTypes();

        $other_questions = '';

        include(SMARTQ_PATH.'/views/question.html.php');
	}
	
	static function edit_question() {
        $_question = new SmartQuizQuestion();
        $_quiz = new SmartQuizQuiz();

        if(!empty($_POST['submit']) and check_admin_referer('smart_quiz_question')) {
            try {
                $_question->update($_POST, $_GET['id']);
                chained_redirect("admin.php?page=smart_quiz_questions&quiz_id=".intval($_POST['quiz_id']));
            }
            catch(Exception $e) {
                $error = $e->getMessage();
            }
        }

        $quiz = $_quiz->getQuiz($_GET['quiz_id']);
        $question_types = $_question->getQuestionTypes();

        if ($_GET['id']) {
            $question = $_question->getQuestion($_GET['id']);
            $quiz = $_quiz->getQuiz($question->quiz_id);

        }

        $other_questions = '';

        include(SMARTQ_PATH.'/views/question.html.php');
	}
	
	static function list_questions() {
        $_question = new SmartQuizQuestion();
        $_quiz = new SmartQuizQuiz();

        if(!empty($_GET['del'])) {
            $_question->delete($_GET['id']);
            smart_quiz_redirect("admin.php?page=smart_quiz_questions&quiz_id=1");
        }

        $questions = $_question->getList($_GET['quiz_id']);
        $quiz = $_quiz->getQuiz($_GET['quiz_id']);
        include(SMARTQ_PATH."/views/questions.html.php");
	}

	static function list_question_types() {
        $_question = new SmartQuizQuestion();

        $question_types = $_question->getQuestionTypes();
	    return json_encode($question_types);
    }

    static function updateQuestionConditions($question_id, $conditions) {
        $_condition = new SmartQuizQuestionConditions();

        $_condition->deleteAllConditions($question_id);

        foreach ($conditions as $item) {
            $params = [
                'question_display_id' => $question_id,
                'question_id' => $item->question_id,
                'answer_id' => $item->answer_id,
            ];
            $_condition->add($params);
        }
        return true;
    }

    static function ajax_add_question($request) {
        $_question = new SmartQuizQuestion();
        $_choice = new SmartQuizChoices();

        $data = [];

        $data['title'] = $request['title'];
        $data['title_font'] = $request['title_font'];
        $data['description'] = $request['description'];
        $data['desc_font'] = $request['desc_font'];
        $data['qtype'] = $request['qtype_id'];
        $data['quiz_id'] = $request['quiz_id'];

        $options = json_decode(stripslashes($request['parameters']));
        unset($options->choices);
        $data['options'] = (array)$options;

        if ($request['question_id'] > 0) {
            $question_id = $_question->update($data, $request['question_id']);
        } else {
            $data['sort_order'] = $_question->countQuestions($request['quiz_id']);
            $question_id = $_question->add($data);
        }

        // update conditions
        self::updateQuestionConditions($question_id, json_decode(stripslashes($request['conditions'])));
        $question_info = $_question->getQuestion($question_id);
        $parameters = json_decode(stripslashes($request['parameters']));

        // update choices
        $choices = $parameters->choices;

//        $_question->deleteAllChoices($question_id);

        if ($question_info->qtype == SmartQuizQuestion::SQUIZ_FINAL_FORM_TYPE) {
            $choice_title = $parameters->final_text;
            $choice_value = '';

            $choice_data = [
                'title' => $choice_title,
                'value' => $choice_value,
                'question_id' => $question_id
            ];

            $_choice->add($choice_data);
        } else {
            if ($request['question_id'] > 0) {
                $_choice->updateAll($choices, $request['question_id']);
            } else {
                foreach ($choices as $choice) {
                    if ($question_info->qtype == SmartQuizQuestion::SQUIZ_TEXT_TYPE ||
                        $question_info->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TEXT_TYPE
                    ) {
                        $choice_title = $choice->title;
                        $choice_value = '';
                    } elseif ($question_info->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TYPE) {
                        $choice_title = $choice->title;
                        $data_val = [
                            'image_id' => $choice->value
                        ];
                        $choice_value = json_encode($data_val);
                    } else {
                        $choice_title = $choice->title;
                        $choice_value = '';
                    }

                    $choice_data = [
                        'title' => $choice_title,
                        'value' => $choice_value,
                        'question_id' => $question_id
                    ];
                    $_choice->add($choice_data);
                }
            }

        }

        return json_encode(['success' => 'Question added/updated']);
    }

    static function ajax_get_question($id) {
        $_question = new SmartQuizQuestion();
        $_choice = new SmartQuizChoices();
        $question = $_question->getQuestion((int) $id);

        if ($question) {
            $choices = $_choice->getList($question->id);
            $options = (array)json_decode($question->options);

            $data = $question;

            if ($question->qtype == SmartQuizQuestion::SQUIZ_FINAL_FORM_TYPE) {
                $data->parameters = [
                    'final_text' => $choices[0]['title'],
                ];
            } else {
                $data->parameters = array_merge(['choices' => $choices], $options);
            }
            return json_encode($data);
        } else {
            return false;
        }

    }

    /**
     * Get questions that lower in sort order
     * @param $id
     * @return mixed
     */
    static function ajax_get_prev_question($id) {
        $_question = new SmartQuizQuestion();

        if ($id > 0) {
            $question = $_question->getQuestionForConditions((int) $id);
        } else {
            $question = $_question->getAllQuestionForConditions();
        }

        return json_encode($question);
    }

    /**
     * Get question display conditions
     * @param $id
     * @return mixed
     */
    static function ajax_get_question_conditions($id) {
        $_question = new SmartQuizQuestion();

        $conditions = $_question->getQuestionConditions((int) $id);

        return json_encode($conditions);
    }

    /**
     * Get questions for table
     * @param $quiz_id
     * @return mixed
     */
    static function ajax_get_question_list($quiz_id) {
        $_question = new SmartQuizQuestion();

        $questions = $_question->getList((int) $quiz_id);

        return json_encode($questions);
    }

    /**
     * Update question sort order
     * @param $request
     * @return mixed
     */
    static function ajax_update_question_sort_order($request) {
        $_question = new SmartQuizQuestion();

        $questions = $_question->updateAllSortOrder($request['quiz_id'], json_decode(stripslashes($request['questions'])));

        return json_encode($questions);
    }
	
}