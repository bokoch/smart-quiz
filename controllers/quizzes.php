<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class SmartQuizQuizzes {

	static function manage()
    {
 		$action = empty($_GET['action']) ? 'list' : $_GET['action'];
		switch($action) {
			case 'add':
				self :: add_quiz();
			break;
			case 'edit': 
				self :: edit_quiz();
			break;
			case 'list':
			default:
				self :: list_quizzes();	 
			break;
		}
	}

	static function add_quiz()
    {
        $_quiz = new SmartQuizQuiz();
        if(!empty($_POST['submit']) and check_admin_referer('smart_quiz')) {
            try {
                $qid = $_quiz->add($_POST);
                smart_quiz_redirect("admin.php?page=smart_quiz_questions&quiz_id=".$qid);
            }
            catch(Exception $e) {
                $error = $e->getMessage();
            }
        }

        include(SMARTQ_PATH.'/views/quiz.html.php');
    }

    static function edit_quiz()
    {
        global $wpdb;
        $_quiz = new SmartQuizQuiz();

        if(!empty($_POST['submit']) and check_admin_referer('smart_quiz')) {
            try {
                $_quiz->save($_POST, $_GET['id']);
                smart_quiz_redirect("admin.php?page=smart_quizzes");
            }
            catch(Exception $e) {
                $error = $e->getMessage();
            }
        }

        // select the quiz
        $quiz = $wpdb->get_row($wpdb->prepare("SELECT * FROM ".SMARTQ_QUIZZES." WHERE id=%d", intval($_GET['id'])));
        $output = stripslashes($quiz->output);

        include(SMARTQ_PATH.'/views/quiz.html.php');
    }

    static function list_quizzes()
    {
        $_quiz = new SmartQuizQuiz();

        if(!empty($_GET['del'])) {
            $_quiz->delete($_GET['id']);
            smart_quiz_redirect("admin.php?page=smart_quizzes");
        }

        $quizzes = $_quiz->getList();
        include(SMARTQ_PATH."/views/smart-quizzes.html.php");

    }

    static function display($quiz_id)
    {
        wp_enqueue_script("squiz-vue");

        include(SMARTQ_PATH."/views/display-quiz.html.php");
    }

    static function answer_question($quiz_result) {
	    $quiz_array = json_decode($quiz_result, true);
	   return self::send_email($quiz_result);
    }

    static function send_email($result_array) {
        $_quiz = new SmartQuizQuiz();
        $quiz_info = $_quiz->getQuiz($result_array['quiz_id']);

        $msg_body = self::make_email($result_array);
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $mail_to = $quiz_info->admin_email;
        if ( mail($mail_to, 'Заявка с сайта', $msg_body, $headers) ) {
            return 'Успешно отправлено';
        } else {
            return 'Произошла ошибка. Попробуйте еще раз';
        }

    }

    static function make_email($result_array) {
	    $msg = '<b>Сообщение от клиента</b><br>';
	    $msg .= '<b>Тема: Результат опроса</b><br>';

	    if (isset($result_array['messenger'])) {
            $msg .= '<b>Мессенджер: </b>' . $result_array['messenger'] . '<br>';
            $msg .= '<b>Номер: </b>' . $result_array['mess_number'] . '<br>';
        } else {
            $msg .= '<b>Имя: </b>' . $result_array['user_name'] . '<br>';
            $msg .= '<b>Номер: </b>' . $result_array['user_phone'] . '<br>';
        }

	    foreach (json_decode(stripslashes($result_array['result']), true) as $index => $item) {
            $msg .= '<b>' . ($index+1) . '.  ' . $item['question_title'] . '</b> Ответ:' . $item['answer'][0]['title'] . '<br>';
        }

        // set utm
        $utm_data = json_decode(stripslashes($result_array['referal_utm']));

        $msg .= '<br><b>Полученное предложение: </b>' . $result_array['final_text'] . '<br>';

        $msg .= '<hr>';
        $msg .= '<b>UTM_SOURCE: </b>' . $utm_data->utm_source . '<br>';
        $msg .= '<b>UTM_MEDIUM: </b>' . $utm_data->utm_medium . '<br>';
        $msg .= '<b>UTM_CAMPAIGN: </b>' . $utm_data->utm_campaign . '<br>';
        $msg .= '<b>UTM_TERM: </b>' . $utm_data->utm_term . '<br>';

        return $msg;
    }

    static function get_quiz_front($quiz_id) {
        $_quiz = new SmartQuizQuiz();

        $quiz = $_quiz->getQuizFront($quiz_id);
        return json_encode($quiz);
    }

}