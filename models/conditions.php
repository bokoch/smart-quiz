<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class SmartQuizQuestionConditions
{

    // Union types
    const SQUIZ_UNION_AND = 'and';
    const SQUIZ_UNION_OR = 'or';

    public function add($vars)
    {
        global $wpdb;

        $question_display_id = (int) $vars['question_display_id'];
        $question_id = (int) $vars['question_id'];
        $answer_id = (int) $vars['answer_id'];
        $union = 'and';

        $result = $wpdb->query($wpdb->prepare("INSERT INTO ".SMARTQ_QUESTION_CONDITIONS." SET
			question_display_id=%d, question_id=%d, answer_id=%d, union_type=%s",
            $question_display_id, $question_id, $answer_id, $union));

        if($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        $condition_id = $wpdb->insert_id;

        return $condition_id;
    }

    public function getCondition($condition_id)
    {
        global $wpdb;

        $conditions = $wpdb->get_results("SELECT * FROM ". SMARTQ_QUESTION_CONDITIONS . " WHERE id=" . (int)$condition_id);
        return $conditions[0];
    }

    public function getConditionsByQuestion($question_id)
    {
        global $wpdb;

        $conditions = $wpdb->get_results("SELECT * FROM ". SMARTQ_QUESTION_CONDITIONS . " WHERE question_id=" . (int) $question_id);

        return $conditions;
    }

    public function deleteAllConditions($question_id)
    {
        global $wpdb;

        $id = intval($question_id);

        // delete all conditions related to this questions
        $wpdb->query($wpdb->prepare("DELETE FROM ".SMARTQ_QUESTION_CONDITIONS." WHERE question_display_id=%d", $id));
    }

    public function delete($id)
    {
        global $wpdb;

        $id = intval($id);

        $wpdb->query($wpdb->prepare("DELETE FROM ".SMARTQ_QUESTION_CONDITIONS." WHERE id=%d", $id));
    }

}