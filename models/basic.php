<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly
// main model containing general config and UI functions
class SmartQuiz
{
    static function install($update = false) {
        global $wpdb;
        $wpdb -> show_errors();
        self::init();

        /**
         * Create quiz table
         */
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUIZZES."'") != SMARTQ_QUIZZES) {
            $sql = "CREATE TABLE `" . SMARTQ_QUIZZES . "` (
				  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `title` VARCHAR(255) NOT NULL DEFAULT '',
				  `admin_email` VARCHAR(255) NOT NULL DEFAULT ''
				) DEFAULT CHARSET=utf8;";

            $wpdb->query($sql);
        }

        /**
         * Create questions table
         */
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUESTIONS."'") != SMARTQ_QUESTIONS) {
            $sql = "CREATE TABLE `" . SMARTQ_QUESTIONS . "` (
				  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `quiz_id` INT(10) NOT NULL,
				  `title` VARCHAR(255) NOT NULL DEFAULT '',
				  `title_font` INT(10),
				  `description` TEXT,
				  `desc_font` INT(10),
				  `qtype` INT(10),
				  `options` TEXT,
				  `sort_order` INT(10)				  
				) DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }

        /**
         * Create question types table
         */
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUESTION_TYPES."'") != SMARTQ_QUESTION_TYPES) {
            $sql = "CREATE TABLE `" . SMARTQ_QUESTION_TYPES . "` (
				  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `title` VARCHAR(255),
				  `value` VARCHAR(255)
				) DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);

            // set basic question types
            $init_insert_sql = "INSERT INTO `" . SMARTQ_QUESTION_TYPES . "` (`id`, `title`, `value`) VALUES
                                (1, 'Варианты ответов', 'text'),
                                (2, 'Варианты с картинками', 'images'),
                                (3, 'Выпадающий список', 'select'),
                                (4, 'Ползунок', 'range'),
                                (5, 'Текст на конечной форме', 'final_form'),
                                (6, 'Варианты и картинка', 'images_text'),
                                (7, 'Дата', 'date');";
            $wpdb->query($init_insert_sql);
        }

        /**
         * Create question-conditions table table
         */
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUESTION_CONDITIONS."'") != SMARTQ_QUESTION_CONDITIONS) {
            $sql = "CREATE TABLE `" . SMARTQ_QUESTION_CONDITIONS . "` (
				  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `question_display_id` INT(10) NOT NULL,
				  `question_id` INT(10) NOT NULL,
				  `answer_id` INT(10) NOT NULL,
				  `union_type` VARCHAR(64) NOT NULL DEFAULT 'and'
				) DEFAULT CHARSET=utf8;";
            $wpdb->query($sql);
        }
        /**
         * Create choices table
         */
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_CHOICES."'") != SMARTQ_CHOICES) {
            $sql = "CREATE TABLE `" . SMARTQ_CHOICES . "` (
				  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `question_id` INT(10) NOT NULL,
				  `title` TEXT,
				  `value` VARCHAR(255),
				  `sort_order` INT(10)
				) DEFAULT CHARSET=utf8;";

            $wpdb->query($sql);
        }

    }

    static function deinstall()
    {
        self::delete_tables();
    }

    static function delete_tables() {
        global $wpdb;

        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUIZZES."'") == SMARTQ_QUIZZES) {
            $sql = "DROP TABLE `" . SMARTQ_QUIZZES . "`";
            $wpdb->query($sql);
        }
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUESTIONS."'") == SMARTQ_QUESTIONS) {
            $sql = "DROP TABLE `" . SMARTQ_QUESTIONS . "`";
            $wpdb->query($sql);
        }
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_CHOICES."'") == SMARTQ_CHOICES) {
            $sql = "DROP TABLE `" . SMARTQ_CHOICES . "`";
            $wpdb->query($sql);
        }
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUESTION_CONDITIONS."'") == SMARTQ_QUESTION_CONDITIONS) {
            $sql = "DROP TABLE `" . SMARTQ_QUESTION_CONDITIONS . "`";
            $wpdb->query($sql);
        }
        if($wpdb->get_var("SHOW TABLES LIKE '".SMARTQ_QUESTION_TYPES."'") == SMARTQ_QUESTION_TYPES) {
            $sql = "DROP TABLE `" . SMARTQ_QUESTION_TYPES . "`";
            $wpdb->query($sql);
        }

    }

    /**
     * Set menu bar
     */
    static function menu()
    {
        $smart_quiz_caps = current_user_can('manage_options') ? 'manage_options' : 'smart_quiz_manage';

        add_menu_page(__('Smart Quiz', 'smart_quiz'), __('Smart Quiz', 'smart_quiz'), $smart_quiz_caps, "smart_quizzes",
            array('SmartQuizQuizzes', "manage"));

        add_submenu_page(NULL, __('Smart Quiz Questions', 'smart_quiz'), __('Chained Quiz Questions', 'smart_quiz'), $smart_quiz_caps,
            'smart_quiz_questions', array('SmartQuizQuestions','manage'));

//        add_submenu_page('smart_quizzes', __('Settings', 'smart_quiz'), __('Settings', 'smart_quiz'), $smart_quiz_caps,
//            'smart_quiz_options', array('Smart Quiz', 'options'));
    }

    /**
     * Set admin style and scripts
     */
    static function admin_scripts()
    {
        // Styles
        wp_register_style(
            'smart-quiz-admin-style',
            SMARTQ_URL . 'css/admin.css'
        );
        wp_enqueue_style( 'admin_style',
            SMARTQ_URL . 'css/admin.css',
            false,
            '1.0.0' );

        wp_enqueue_script(
            'smart-quiz-swal',
            SMARTQ_URL . 'js/sweetalert2.all.min.js'
        );

        wp_enqueue_script(
            'smart-quiz-admin',
            SMARTQ_URL . 'js/admin.js'
        );

        wp_enqueue_script(
            'smart-quiz-admin-vue',
            SMARTQ_URL . 'squiz/dist/squiz-admin-app.js'
        );


        wp_enqueue_media();

        do_action('wp_plupload_default_settings');

    }

    /**
     * Load scripts and styles
     */
    static function scripts()
    {

        // Javascript
        wp_enqueue_script('jquery');

        wp_register_script(
            'smart-quiz-common',
            SMARTQ_URL . 'js/common.js',
            false,
            '1.0.0',
            false
        );
        wp_enqueue_script("smart-quiz-common");

        wp_register_script(
            'squiz-vue',
            SMARTQ_URL . 'squiz/dist/squiz-app.js',
            false,
            '1.0.0',
            false
        );
//        wp_enqueue_script("squiz-vue");

    }

    /**
     * Initialize variables and plugin settings
     */
    static function init() {
        global $wpdb;
        // define table names
        define( 'SMARTQ_QUIZZES', $wpdb->prefix. "smart_quiz_quizzes");
        define( 'SMARTQ_QUESTIONS', $wpdb->prefix. "smart_quiz_questions");
        define( 'SMARTQ_CHOICES', $wpdb->prefix. "smart_quiz_choices");
        define( 'SMARTQ_QUESTION_CONDITIONS', $wpdb->prefix. "smart_question_conditions");
        define( 'SMARTQ_QUESTION_TYPES', $wpdb->prefix. "smart_question_types");

        // shortcodes
        add_shortcode('smart-quiz', array("SmartQuizShortcodes", "quiz"));
    }

    /*static function help() {
        require(CHAINED_PATH."/views/help.php");
    }*/
}