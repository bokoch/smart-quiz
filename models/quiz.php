<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly
class SmartQuizQuiz
{
    function add($vars)
    {
        global $wpdb;
        $title = sanitize_text_field($vars['title']);
        $admin_email = sanitize_text_field($vars['admin_email']);

        $result = $wpdb->query($wpdb->prepare("INSERT INTO " . SMARTQ_QUIZZES . " SET
			title=%s, admin_email=%s",
            $title, $admin_email));

        if ($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        $quiz_id = $wpdb->insert_id;

        return $quiz_id;
    }

    function save($vars, $id)
    {
        global $wpdb;

        $id = intval($id);
        $title = sanitize_text_field($vars['title']);
        $admin_email = sanitize_text_field($vars['admin_email']);

        $result = $wpdb->query($wpdb->prepare("UPDATE " . SMARTQ_QUIZZES . " SET
			title=%s, admin_email=%s 
			WHERE id=%d",
            $title, $admin_email, $id));

        if ($result === false) throw new Exception(__('DB Error', 'chained'));

        return true;
    }

    function delete($id)
    {
        global $wpdb;

        $id = intval($id);

        $wpdb->query($wpdb->prepare("DELETE FROM " . SMARTQ_QUIZZES . " WHERE id=%d", $id));

    }

    function getList()
    {
        global $wpdb;

        $quizzes = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUIZZES);

        return $quizzes;
    }

    function getQuiz($quiz_id)
    {
        global $wpdb;

        $quiz = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUIZZES . " WHERE id=" . (int)$quiz_id);
        return $quiz[0];
    }

    function getQuizFront($quiz_id)
    {
        global $wpdb;
        $quiz = [];

        $result = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUIZZES . " WHERE id=" . (int)$quiz_id)[0];

        $quiz['id'] = $result->id;
        $quiz['title'] = $result->title;

        $quiz['questions'] = $this->getQuestionsFront($quiz_id);

        return $quiz;
    }

    private function getQuestionsFront($quiz_id)
    {
        global $wpdb;

        $questions = [];

        $results = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS . " WHERE quiz_id=" . (int)$quiz_id . " ORDER BY sort_order ASC");

        foreach ($results as $question) {

            $options = (array)json_decode($question->options);
            if (!empty($options['isOthers'])) {
                $options['otherAnswer'] = '';
            }

            $params = [
                'title' => $question->title,
                'title_font' => $question->title_font,
                'userAnswer' => null,
                'answers' => $this->getQuestionAnswersFront($question),
                'text' => $question->description,
                'text_font' => $question->desc_font,
                'id' => $question->id,
                'type' => $this->getQuestionTypeById($question->qtype),
                'display' => $this->getQuestionConditionsFront($question)
            ];

            $questions[] = array_merge($options, $params);
        }

        return $questions;

    }

    private function getQuestionConditionsFront($question)
    {
        global $wpdb;
        $results = $wpdb->get_results("SELECT DISTINCT question_id FROM " . SMARTQ_QUESTION_CONDITIONS . " WHERE question_display_id=" . (int)$question->id . " ORDER BY id ASC");

        $conditions = [];
        foreach ($results as $result) {
            $conditions[] = [
                'union' => $question->union_type,
                'question' => $result->question_id,
                'answer' => $this->getQuestionConditionItemFront($question->id, $result->question_id),
            ];
        }

        return ['conditions' => $conditions];
    }

    private function getQuestionConditionItemFront($question_display_id, $question_id)
    {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTION_CONDITIONS . " WHERE question_display_id=" . (int)$question_display_id . " AND question_id=" . (int)$question_id . " ORDER BY id ASC");

        $answers = [];
        foreach ($results as $result) {
            array_push($answers, $result->answer_id);
        }

        return $answers;
    }

    private function getQuestionAnswersFront($question)
    {
        global $wpdb;

        $choices = $wpdb->get_results("SELECT * FROM " . SMARTQ_CHOICES . " WHERE question_id=" . (int)$question->id . " ORDER BY sort_order ASC");

        $choices_format = [];
        foreach ($choices as $choice) {

            if ($question->qtype == SmartQuizQuestion::SQUIZ_TEXT_TYPE) {
                $choice_title = $choice->title;
                $choice_value = '';
            } elseif ($question->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TYPE
                    || $question->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TEXT_TYPE) {
                $choice_title = $choice->title;

                $choice_val_params = json_decode($choice->value);

                $choice_data['value'] = wp_get_attachment_image_src($choice_val_params->image_id, 'large')[0];
                $choice_value = $choice_val_params->image_id;
            } else {
                $choice_title = $choice->title;
                $choice_value = '';
            }

            $choice_data['title'] = $choice_title;
            $choice_data['id'] = $choice->id;

            array_push($choices_format, $choice_data);
        }

        return $choices_format;
    }

    private function getQuestionTypeById($type_id)
    {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTION_TYPES . " WHERE id=" . (int)$type_id)[0];
        return $result->value;
    }
}