<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly
class SmartQuizQuestion
{

    // Question types
    const SQUIZ_TEXT_TYPE = 1;
    const SQUIZ_IMAGE_TYPE = 2;
    const SQUIZ_SELECT_TYPE = 3;
    const SQUIZ_RANGE_TYPE = 4;

    /**
     * Text of this type will shown in final form,
     * as final message
     */
    const SQUIZ_FINAL_FORM_TYPE = 5;
    const SQUIZ_IMAGE_TEXT_TYPE = 6;
    const SQUIZ_DATE_TYPE = 7;
    const SQUIZ_FIELD_TYPE = 8;

    public function add($vars)
    {
        global $wpdb;
        $quiz_id = (int)$vars['quiz_id'];
        $sort_order = (int)$vars['sort_order'];
        $title_font = (int)$vars['title_font'];
        $desc_font = (int)$vars['desc_font'];
        $options = json_encode($vars['options']);
        $result = $wpdb->query($wpdb->prepare("INSERT INTO " . SMARTQ_QUESTIONS . " SET
			title=%s, title_font=%d, description=%s, desc_font=%d, qtype=%s, options=%s, quiz_id=%d, sort_order=%d",
            $vars['title'], $title_font, $vars['description'], $desc_font, $vars['qtype'], $options, $quiz_id, $sort_order));

        if ($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        $quiz_id = $wpdb->insert_id;

        return $quiz_id;
    }

    public function update($vars, $id)
    {
        global $wpdb;

        $quiz_id = (int)$vars['quiz_id'];
        $sort_order = (int)$vars['sort_order'];
        $title_font = (int)$vars['title_font'];
        $desc_font = (int)$vars['desc_font'];

        $options = json_encode($vars['options']);

        if (!current_user_can('unfiltered_html')) {
            $vars['question'] = strip_tags($vars['question']);
        }

        $result = $wpdb->query($wpdb->prepare("UPDATE " . SMARTQ_QUESTIONS . " SET
			title=%s, title_font=%d, description=%s, desc_font=%d, qtype=%s, options=%s, quiz_id=%d WHERE id=%d",
            $vars['title'], $title_font, $vars['description'], $desc_font, $vars['qtype'], $options, $quiz_id, (int)$id));

        if ($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        return $id;
    }

    public function updateOrder($vars, $id)
    {
        global $wpdb;

        $sort_order = (int)$vars['sort_order'];

        if (!current_user_can('unfiltered_html')) {
            $vars['question'] = strip_tags($vars['question']);
        }

        $result = $wpdb->query($wpdb->prepare("UPDATE " . SMARTQ_QUESTIONS . " SET
			sort_order=%d WHERE id=%d",
            $sort_order, (int)$id));

        if ($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        return $id;
    }

    public function getQuestion($question_id)
    {
        global $wpdb;

        $question = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS . " WHERE id=" . (int)$question_id)[0];
        return $question;
    }

    /**
     * Get questions with choices that lower in sort order
     * @param $question_id
     * @return mixed
     */
    public function getQuestionForConditions($question_id)
    {
        global $wpdb;

        $question = $this->getQuestion((int)$question_id);
        $data = [];

        if (!empty($question)) {
            $conditionQuestionList = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS . " WHERE sort_order < " . (int)$question->sort_order);
            $condition_model = new SmartQuizChoices();

            $data = [];
            foreach ($conditionQuestionList as $item) {
                $data[] = [
                    'id' => $item->id,
                    'label' => $item->title,
                    'choices' => $this->getQuestionChoices((int)$item->id)
                ];
            }
        }

        return $data;
    }

    public function getAllQuestionForConditions()
    {
        global $wpdb;
        $data = [];

        $conditionQuestionList = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS);
        $condition_model = new SmartQuizChoices();

        $data = [];
        foreach ($conditionQuestionList as $item) {
            $data[] = [
                'id' => $item->id,
                'label' => $item->title,
                'choices' => $this->getQuestionChoices((int)$item->id)
            ];
        }

        return $data;
    }

    /**
     * Get question display conditions
     * @param $question_id
     * @return mixed
     */
    public function getQuestionConditions($question_id)
    {
        global $wpdb;

        $data = [];

        $conditions = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTION_CONDITIONS . " WHERE question_display_id = " . (int)$question_id);

        foreach ($conditions as $item) {
            $data[] = [
                'question_id' => $item->question_id,
                'answer_id' => $item->answer_id,
            ];
        }

        return $data;
    }

    public function getQuestionChoices($question_id)
    {
        global $wpdb;

        $choices = $wpdb->get_results("SELECT * FROM " . SMARTQ_CHOICES . " WHERE question_id=" . (int)$question_id . " ORDER BY id ASC");

        $data = [];
        foreach ($choices as $item) {
            $data[] = [
                'id' => $item->id,
                'label' => $item->title
            ];
        }

        return $data;
    }

    public function getQuestionTypes()
    {
        global $wpdb;

        $question_types = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTION_TYPES);

        return $question_types;
    }

    public function getList($quiz_id)
    {
        global $wpdb;

        $questions = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS . " WHERE quiz_id=" . (int)$quiz_id . " ORDER BY sort_order ASC");

        return $questions;
    }

    public function delete($id)
    {
        global $wpdb;

        $id = intval($id);

        // delete all conditions related to this questions
        $wpdb->query($wpdb->prepare("DELETE FROM " . SMARTQ_QUESTION_CONDITIONS . " WHERE question_id=%d", $id));
        $wpdb->query($wpdb->prepare("DELETE FROM " . SMARTQ_CHOICES . " WHERE question_id=%d", $id));
        $wpdb->query($wpdb->prepare("DELETE FROM " . SMARTQ_QUESTIONS . " WHERE id=%d", $id));
    }

    public function deleteAllChoices($question_id)
    {
        global $wpdb;

        $id = intval($question_id);

        // delete all choices related to this questions
        $wpdb->query($wpdb->prepare("DELETE FROM " . SMARTQ_CHOICES . " WHERE question_id=%d", $id));
    }

    public function countQuestions($quiz_id)
    {
        global $wpdb;

        $id = intval($quiz_id);

        $count_questions = $wpdb->get_results("SELECT COUNT(*) as count_questions FROM " . SMARTQ_QUESTIONS . " WHERE quiz_id=" . $id);
        return $count_questions[0]->count_questions;
    }

    public function updateAllSortOrder($quiz_id, $questions)
    {
        global $wpdb;

        foreach ($questions as $key => $val) {
            $checkQuestion = $this->checkQuestion($quiz_id, $val->id);
            if (!empty($checkQuestion)) {
                $this->updateSortOrder($key, $checkQuestion);
            }
        }

        return $this->getList($quiz_id);
    }

    public function checkQuestion($quiz_id, $question_id)
    {
        global $wpdb;

        $question = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS . " WHERE id=" . (int)$question_id . " AND quiz_id=" . (int)$quiz_id);
        return $question[0];
    }

    public function updateSortOrder($order, $question)
    {
        global $wpdb;

        $result = $wpdb->query($wpdb->prepare("UPDATE " . SMARTQ_QUESTIONS . " SET
			sort_order=%d WHERE id=%d",
            (int)$order, (int)$question->id));

        if ($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        return true;
    }

}