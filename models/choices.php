<?php
if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class SmartQuizChoices {

    public function add($vars)
    {
        global $wpdb;
        $question_id = (int) $vars['question_id'];
        $sort_order = $this->countChoices($question_id);

        $result = $wpdb->query($wpdb->prepare("INSERT INTO ".SMARTQ_CHOICES." SET
			title=%s, value=%s, question_id=%d, sort_order=%d",
            $vars['title'], $vars['value'], $question_id, $sort_order));

        if($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        $choice_id = $wpdb->insert_id;

        return $choice_id;
    }

    public function countChoices($question_id)
    {
        global $wpdb;

        $id = intval($question_id);

        $count_choices = $wpdb->get_results("SELECT COUNT(*) as count_questions FROM ".SMARTQ_CHOICES." WHERE question_id=" . $id);
        return $count_choices[0]->count_questions;
    }

    public function update($vars, $id)
    {
        global $wpdb;

        $question_id = (int) $vars['question_id'];
        $sort_order = (int) $vars['sort_order'];

        $result = $wpdb->query($wpdb->prepare("UPDATE ".SMARTQ_CHOICES." SET
			title=%s, value=%s, question_id=%d, sort_order=%d WHERE id=%d",
            $vars['title'], $vars['value'], $question_id, $sort_order, (int)$id));

        if($result === false) throw new Exception(__('DB Error', 'smart_quiz'));

        return $id;
    }

    public function updateAll($choices, $question_id)
    {
        global $wpdb;

        $curr_choices = $this->getList($question_id);

        $question_info = $wpdb->get_results("SELECT * FROM " . SMARTQ_QUESTIONS . " WHERE id=" . (int)$question_id)[0];

        $curr_ids = [];
        $choices_ids = [];
        foreach ($curr_choices as $item) {
            array_push($curr_ids, $item['id']);
        }
        foreach ($choices as $item) {
            array_push($choices_ids, $item->id);
        }

        $remove_ids = array_diff($curr_ids, $choices_ids);
        foreach ($remove_ids as $remove_id) {
            $this->delete($remove_id);
        }
        foreach ($choices as $key => $choice) {
            if ($question_info->qtype == SmartQuizQuestion::SQUIZ_TEXT_TYPE) {
                $choice_value = '';
            } elseif ($question_info->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TYPE
                || $question_info->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TEXT_TYPE
            ) {
                $data_val = [
                    'image_id' => $choice->value
                ];
                $choice_value = json_encode($data_val);
            } else {
                $choice_value = '';
            }

            $choice->sort_order = $key;
            $choice->value = $choice_value;

            if ($choice->id > 0) {
                $this->update((array)$choice, $choice->id);
            } else {
                $choice->question_id = $question_id;
                $this->add((array)$choice);
            }
        }
        return $question_id;
    }

    public function getChoice($choice_id)
    {
        global $wpdb;

        $question = $wpdb->get_results("SELECT * FROM ". SMARTQ_CHOICES . " WHERE id=" . (int)$choice_id);
        return $question[0];
    }

    public function getList($question_id)
    {
        global $wpdb;

        $question =  $wpdb->get_results("SELECT * FROM ". SMARTQ_QUESTIONS . " WHERE id=" . (int)$question_id)[0];

        $choices = $wpdb->get_results("SELECT * FROM ". SMARTQ_CHOICES . " WHERE question_id=" . (int) $question_id . " ORDER BY sort_order ASC");

        $choices_format = [];
        foreach ($choices as $choice) {

            $choice_data = [
                'question_id' => $choice->question_id
            ];

            if ($question->qtype == SmartQuizQuestion::SQUIZ_TEXT_TYPE) {
                $choice_title = $choice->title;
                $choice_value = '';
            } elseif ($question->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TYPE
                        || $question->qtype == SmartQuizQuestion::SQUIZ_IMAGE_TEXT_TYPE
            ) {
                $choice_title = $choice->title;

                $choice_val_params = json_decode($choice->value);

                $choice_data['image'] = wp_get_attachment_image_src($choice_val_params->image_id, 'large')[0];
                $choice_value = $choice_val_params->image_id;
            } else {
                $choice_title = $choice->title;
                $choice_value = '';
            }

            $choice_data['id'] = $choice->id;
            $choice_data['title'] = $choice_title;
            $choice_data['value'] = $choice_value;

            array_push($choices_format, $choice_data);
        }

        return $choices_format;
    }

    public function delete($id)
    {
        global $wpdb;

        $id = intval($id);

        $wpdb->query($wpdb->prepare("DELETE FROM ".SMARTQ_CHOICES." WHERE id=%d", $id));
        $wpdb->query($wpdb->prepare("DELETE FROM ".SMARTQ_QUESTION_CONDITIONS." WHERE answer_id=%d", $id));
    }

}