<?php
/*
Plugin Name: Smart Quiz
*/

define( 'SMARTQ_PATH', dirname( __FILE__ ) );
define( 'SMARTQ_RELATIVE_PATH', dirname( plugin_basename( __FILE__ )));
define( 'SMARTQ_URL', plugin_dir_url( __FILE__ ));

// require controllers and models
require_once(SMARTQ_PATH.'/models/basic.php');
require_once(SMARTQ_PATH.'/controllers/quizzes.php');
require_once(SMARTQ_PATH.'/controllers/questions.php');
require_once(SMARTQ_PATH.'/controllers/ajax.php');
require_once(SMARTQ_PATH.'/controllers/shortcodes.php');
require_once(SMARTQ_PATH.'/models/quiz.php');
require_once(SMARTQ_PATH.'/models/question.php');
require_once(SMARTQ_PATH.'/models/choices.php');
require_once(SMARTQ_PATH.'/models/conditions.php');
require_once(SMARTQ_PATH.'/helpers/htmlhelper.php');


add_action('init', array("SmartQuiz", "init"));

register_activation_hook(__FILE__, array("SmartQuiz", "install"));
register_deactivation_hook(__FILE__, array("SmartQuiz", "deinstall"));
add_action('admin_menu', array("SmartQuiz", "menu"));
add_action('admin_enqueue_scripts', array("SmartQuiz", "scripts"));

// show the things on the front-end
add_action( 'wp_enqueue_scripts', array("SmartQuiz", "scripts"));
add_action( 'admin_enqueue_scripts', array("SmartQuiz", "admin_scripts") );


// other actions
add_action('wp_ajax_smartquiz_ajax', 'smartquiz_ajax');
add_action('wp_ajax_nopriv_smartquiz_ajax', 'smartquiz_ajax');