import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        questionTypes: [],
        question: {
            qtype: '',
            quiz_id: '',
            title: '',
            title_font: 28,
            question: '',
            desc_font: 16,
            description: ''
        },
        currentTypeIndex: 0,
        showModal: false,
        questionList: [],
        conditions: [],
        questionsTableList: []
    },

    actions: {
        getQuestionType(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setQuestionTypes', response.data);
                })
                .catch(error => {
                    console.log(error);
                });

        },
        getQuestionParams(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);
            form_data.append('question_id', params.question_id);

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setQuestion', response.data);
                    context.commit('setCurrentTypeIndex', parseInt(context.state.questionTypes.findIndex((item) => item.id === context.state.question.qtype.toString())));
                })
                .catch(error => {
                    console.log(error);
                });

        },
        getConditionQuestionList(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);
            form_data.append('question_id', params.question_id);

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setQuestionList', response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        getConditions(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);
            form_data.append('question_id', params.question_id);

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setConditions', response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        getQuestionsTableList(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);
            form_data.append('quiz_id', params.quiz_id);

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setQuestionsTableList', response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        updateQuestionSortOrder(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);
            form_data.append('quiz_id', params.quiz_id);
            form_data.append('questions', JSON.stringify(params.questions));

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setQuestionsTableList', response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    },

    mutations: {
        setQuestionTypes(state, data) {
            state.questionTypes = data;
        },
        setQuestion(state, data) {
            state.question = data;
        },
        setShowModal(state, data) {
            state.showModal = data;
        },
        setConditions(state, data) {
            state.conditions = data;
        },
        setQuestionList(state, data) {
            state.questionList = data;
        },
        setCurrentTypeIndex(state, data) {
            state.currentTypeIndex = data;
        },
        setQuestionsTableList(state, data) {
            state.questionsTableList = data;
        },
    }
})