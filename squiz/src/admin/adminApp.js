import Vue from "vue";
import store from "./store/admin-store";
import App from './components/App'
import ListApp from './components/ListApp'
import Vuelidate from 'vuelidate'
import VTooltip from 'v-tooltip'

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
Vue.config.productionTip = false
Vue.use(Vuelidate)
Vue.use(VTooltip)

window.onload = function () {
    // Vue.component('app-question', App);

    if (document.querySelector('#squiz-choices') !== null) {
        new Vue({
            el: '#squiz-choices',
            store,
            components: {
                'app-question': App
            }
        });
    }

    if (document.querySelector('#squiz-questions') !== null) {
        new Vue({
            el: '#squiz-questions',
            store,
            components: {
                'list-question': ListApp
            }
        });
    }

};