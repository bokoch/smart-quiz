import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        quizData: {
            title: "Подбор программы обучения",

            "questions": [
                {
                    "title": "Наше расположение Вас устраивает?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Да",
                            "id": "bc8d632c-5c98-478f-893e-39d60d96a1db",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535559003/gsbwtxklskptymcudxbh.png"
                        },
                        {
                            "title": "Нет",
                            "id": "3c19f3ec-e42f-460a-b075-199743519da5",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535559368/pq1vxaekzhkbix2v9jx0.png"
                        }
                    ],
                    "text": "Мы находимся по адресу Московская обл., село Ромашково, ул. Центральная, 49. До нас удобно добираться как на автомобиле, так и общественным транспортом:\nОт метро Молодежная:\t\n- маршрутка N 597 до остановки улица Центральная. Остановка маршрутки 597 на ул. Ярцевская рядом с выходом из метро;\n- маршрутке N 1218. Остановка маршрутки 1218 на улице Ярцевская рядом с ТЦ Трамплин.",
                    "id": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                    "type": "images",
                    "other": false,
                },
                {
                    "title": "Ваш возраст?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "8-12 лет",
                            "id": "4ab88b9b-5c5c-43c5-b365-41ae795e273e",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535635904/wdzm1lhfgtxmirys7922.jpg"
                        },
                        {
                            "title": "12-16 лет",
                            "id": "b40d7599-629b-40e2-ae64-b6b03184c12d",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535635774/q6ikrvrbozkslrpfn2mh.jpg"
                        },
                        {
                            "title": "16-20 лет",
                            "id": "4c035189-1ae8-4aaa-bab1-41c8f929ed02",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535635784/wyfbyvrblzpzqdwkrho9.jpg"
                        },
                        {
                            "title": "Более 20 лет",
                            "id": "188b190e-50e2-4658-9493-2fd25fd1d28c",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535635786/plhcdst9oezf0atifudu.jpg"
                        }
                    ],
                    "text": "От возраста всадника зависит программа и интенсивность занятий.",
                    "id": "44a8bbf2-b2cc-405e-b825-d7f4eaa1a132",
                    "type": "images",
                    "other": false,
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Какой примерный вес всадника?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "до 40 кг",
                            "id": "dc790e3b-d27c-492c-a79b-1c12549d90cf",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721718/o2tuhbsgdjrunegpa1mf.jpg"
                        },
                        {
                            "title": "40-80 кг",
                            "id": "3875e659-f806-4fb8-a97c-864e4480f13a",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721721/xk33gioj3abkve0iisin.jpg"
                        },
                        {
                            "title": "80-100 кг",
                            "id": "73ba9f29-b7a6-4713-9727-1aa9d8cb6efe",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721750/xv4ppmyptj52mrtofg9o.jpg"
                        },
                        {
                            "title": "от 100 кг",
                            "id": "7fed9cd1-e187-42a3-957c-0454000b9fe4",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721794/y0k2iqkwm6uhc5877qur.jpg"
                        }
                    ],
                    "text": "От веса всадника зависит выбор лошади.",
                    "id": "3eae3ede-f478-4303-9fec-e73c5ad3cd8f",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Вы имеете опыт занятий?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Да",
                            "id": "3267862f-1b05-413f-a8ed-4b1d1b2ce32a",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535722808/rgjmjcpfzd2801byz6bb.jpg"
                        },
                        {
                            "title": "Нет",
                            "id": "6cf31102-1f24-48fd-af7b-4f6746fa1c81",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721569/yz3eigp8bn3m4ugegqyw.jpg"
                        }
                    ],
                    "text": "От опыта всадника зависит подбор программы тренировок, выбор лошади и скидка на занятия.",
                    "id": "04d88fab-6d56-4105-91ed-ad7262c4d351",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Какой опыт Вы имеете?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "До 1 года",
                            "id": "9cbbf532-0ff8-4d87-813d-0da30cfaabdd",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721612/fo8aifl5qlhvk4lwzjhc.jpg"
                        },
                        {
                            "title": "1-3 года",
                            "id": "145d2355-3ec4-42fb-8c9c-da28681f1311",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721636/kkjcdksy55bb5dermne9.jpg"
                        },
                        {
                            "title": "от 3-х лет",
                            "id": "39594b47-1eae-45d9-aae6-01a9bece3ae5",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535721631/nflz7kypjqsfkxu0epno.jpg"
                        }
                    ],
                    "text": "",
                    "id": "c5786598-3105-4a6b-9b77-2b485623a8f0",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "04d88fab-6d56-4105-91ed-ad7262c4d351",
                                "answer": [
                                    "3267862f-1b05-413f-a8ed-4b1d1b2ce32a"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Имеете ли Вы какое то звание или разряд?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Да, имею",
                            "id": "76fc6db9-5d8c-48cb-9043-10d6bb15fce8",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723276/mfaeavr7reuocdhz0mtr.jpg"
                        },
                        {
                            "title": "Нет",
                            "id": "99259a32-7ba6-4412-9ac1-df0c0b8bf541",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723258/wexr5wjk8g5xqkmmmnds.png"
                        }
                    ],
                    "text": "",
                    "id": "d53e1f64-559b-4e83-8da6-8c7fc90acd09",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "04d88fab-6d56-4105-91ed-ad7262c4d351",
                                "answer": [
                                    "3267862f-1b05-413f-a8ed-4b1d1b2ce32a"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Есть ли у Вас своя лошадь?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Да",
                            "id": "16286605-8001-42e7-b96d-0fde277b2718",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723303/g9ahejfcypvawce5qad2.jpg"
                        },
                        {
                            "title": "Нет",
                            "id": "f97d32f7-4f61-4cfb-851b-510a8fdea50b",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723303/g9ahejfcypvawce5qad2.jpg"
                        }
                    ],
                    "text": "",
                    "id": "6be02288-de8c-4e4c-8352-d6886abaf469",
                    "type": "images",
                    "notRequired": false,
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "04d88fab-6d56-4105-91ed-ad7262c4d351",
                                "answer": [
                                    "3267862f-1b05-413f-a8ed-4b1d1b2ce32a"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Мы рады предложить Вам услуги берейтора и постой для Вашей лошади",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Отлично",
                            "id": "f45bb10c-81c0-4b6f-ac98-5defb0bcb0d0",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723394/gepwpelhu1zbzy8qgkky.jpg"
                        },
                        {
                            "title": "Мне не интересно",
                            "id": "68c28d2b-df39-40be-b3c7-d38a55616aa9",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723398/gyjqwmvvbv8vxdmb1wva.png"
                        }
                    ],
                    "text": "В этом случае занятия с Вами на Вашей лошади будут бесплатными",
                    "id": "7e830ae0-af96-4da9-8859-7330d94b4d73",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "6be02288-de8c-4e4c-8352-d6886abaf469",
                                "answer": [
                                    "16286605-8001-42e7-b96d-0fde277b2718"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Что Вы ожидаете от занятий?",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Занятия для оздоровления",
                            "id": "f1960448-db95-450c-80ab-8e9db2346de9",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723428/kehnqbimcmk3m8gan4x9.jpg"
                        },
                        {
                            "title": "Получить разряд или звание",
                            "id": "dbaa9c1d-bc9c-48fe-ba05-3a7c28a5f0ff",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535723470/dkf3g9vldwdizjakzyzw.jpg"
                        }
                    ],
                    "text": "",
                    "id": "ce617953-2643-4db2-9a3f-86616c002444",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "04d88fab-6d56-4105-91ed-ad7262c4d351",
                                "answer": [
                                    "6cf31102-1f24-48fd-af7b-4f6746fa1c81"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Как часто Вы можете заниматься?",
                    "userAnswer": null,
                    "select": "one",
                    "answers": [
                        {
                            "title": "Один раз в неделю",
                            "id": "f1451242-ce84-43c7-9234-bced3d00b634",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535813925/gfxdzqx5jk6jrpgbi6s4.jpg"
                        },
                        {
                            "title": "Два раза в неделю",
                            "id": "39790190-66eb-4840-97c8-60d6a408f5b1",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535813930/ealkdondgux2xdhyhkzx.jpg"
                        },
                        {
                            "title": "Три раза в неделю",
                            "id": "b474aca5-e8e1-4cb7-aa33-65ada35020b8",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535813937/pkbjxhlz9nzuut7uyu7m.jpg"
                        },
                        {
                            "title": "Чаще, чем 3 раза в неделю",
                            "id": "03438fb2-728e-41fa-8f63-8fc47d5ba6ca",
                            "value": "http://res.cloudinary.com/hgwipn3sa/image/upload/v1535813946/bgpamk90fmtqcpquj3ip.jpg"
                        }
                    ],
                    "text": "",
                    "id": "cbb48b57-8011-4d3c-bad1-2ab6f0d0eeb0",
                    "type": "images",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "04d88fab-6d56-4105-91ed-ad7262c4d351",
                                "answer": [
                                    "6cf31102-1f24-48fd-af7b-4f6746fa1c81"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Оставьте контакты, чтобы сохранить скидку 20% на первое занятие!",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Я подумаю",
                            "id": "bad2963b-2066-4c1d-ad84-12ab9826a3c0"
                        }
                    ],
                    "text": "Спасибо, что посетили наш сайт!",
                    "id": "f9268c9c-c315-4096-a2ca-0251fcd4aebc",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "3c19f3ec-e42f-460a-b075-199743519da5"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Наши лошади с отличным стартовым опытом помогут Вам получить или повысить разряд! На первое пробное занятие - скидка 20%",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "15cd39f4-0c04-4696-812f-3b8d60388d82"
                        }
                    ],
                    "text": "Оставьте свой номер телефона на следующем шаге",
                    "id": "186e7837-de50-4be9-b28f-6635ee1e3a44",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "6be02288-de8c-4e4c-8352-d6886abaf469",
                                "answer": [
                                    "f97d32f7-4f61-4cfb-851b-510a8fdea50b"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Мы предлагаем Вам полный пансион для Вашего компаньона, в который входит: индивидуальные тренировки – неограниченное количество, тренер, коновод, индивидуальный шкафчик, круглосуточное видеонаблюдение! ",
                    "userAnswer": null,
                    "select": "one",
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "05f23a9e-c638-4f5a-a808-2b2c87174aa7"
                        }
                    ],
                    "text": "Оставьте Ваш номер телефона на следующем шаге, мы расскажем Вам подробности!",
                    "id": "a4f2ccda-b1c7-4073-9d95-15978783b6c5",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "6be02288-de8c-4e4c-8352-d6886abaf469",
                                "answer": [
                                    "16286605-8001-42e7-b96d-0fde277b2718"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "7e830ae0-af96-4da9-8859-7330d94b4d73",
                                "answer": [
                                    "f45bb10c-81c0-4b6f-ac98-5defb0bcb0d0"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Предлагаем Вам приехать и познакомиться с нашими тренерами, а так же посмотреть условия содержания лошадей. ",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "fd79cba7-1146-4a0b-87c5-2cfe3fa08de5"
                        }
                    ],
                    "text": "Оставьте Ваш номер телефона на следующем шаге, мы расскажем Вам подробности! ",
                    "id": "1afd5c2d-15ac-47ec-b9cc-372adcc5b671",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "6be02288-de8c-4e4c-8352-d6886abaf469",
                                "answer": [
                                    "16286605-8001-42e7-b96d-0fde277b2718"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "7e830ae0-af96-4da9-8859-7330d94b4d73",
                                "answer": [
                                    "68c28d2b-df39-40be-b3c7-d38a55616aa9"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Предлагаем Вам скидку на первое пробное занятие 20%, индивидуальный шкафчик, экипировку для первых занятий. В первый месяц регулярных тренировок, Вы увидите результаты, а так же почувствуете на себе положительное влияние лошади на Ваше физическое и эмоциональное состояние. ",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "ecae9b0b-f7bf-4504-9374-a0398201abe8"
                        }
                    ],
                    "text": "Оставьте Ваш номер телефона на следующем шаге и с вами свяжется Администратор.",
                    "id": "46140325-a40c-4332-a982-9e4203289214",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "cbb48b57-8011-4d3c-bad1-2ab6f0d0eeb0",
                                "answer": [
                                    "f1451242-ce84-43c7-9234-bced3d00b634"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Предлагаем Вам  скидку на абонемент  10 занятий со скидкой 15%, индивидуальный шкафчик, экипировку для первых занятий. В первый месяц регулярных тренировок, Вы увидите результаты, а так же почувствуете на себе положительное влияние лошади на Ваше физическое и эмоциональное состояние!",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "317c2995-0887-494d-84bc-f36dd29522e4"
                        }
                    ],
                    "text": "За Вами остаётся только выбор дня и времени! Оставьте Ваш номер телефона, и с вами свяжется Администратор",
                    "id": "e09ffc62-76ad-42f1-a200-d7343feb4c50",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "cbb48b57-8011-4d3c-bad1-2ab6f0d0eeb0",
                                "answer": [
                                    "39790190-66eb-4840-97c8-60d6a408f5b1"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "Предлагаем Вам  скидку на абонемент  15 занятий со скидкой 18%, индивидуальный шкафчик, экипировку для первых занятий. В первый месяц регулярных тренировок, Вы увидите результаты, а так же почувствуете на себе положительное влияние лошади на Ваше физическое и эмоциональное состояние!",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "f13af307-3119-40a7-8c03-b3aa7f3e18ff"
                        }
                    ],
                    "text": "Оставьте Ваш номер телефона на следующем шаге и с вами свяжется Администратор",
                    "id": "35ad577d-8ccb-4afe-925e-e6ee793e42d7",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "cbb48b57-8011-4d3c-bad1-2ab6f0d0eeb0",
                                "answer": [
                                    "b474aca5-e8e1-4cb7-aa33-65ada35020b8"
                                ]
                            }
                        ]
                    }
                },
                {
                    "title": "У нас для Вас уникальное предложение! Предлагаем Вам  скидку на абонемент  15 занятий со скидкой 18%, индивидуальный шкафчик, экипировку для первых занятий. В первый месяц регулярных тренировок, Вы увидите результаты, а так же почувствуете на себе положительное влияние лошади на Ваше физическое и эмоциональное состояние! За Вами остаётся только выбор дня и времени! ",
                    "userAnswer": null,
                    "answers": [
                        {
                            "title": "Хорошо",
                            "id": "f13af307-3119-40a7-8c03-b3aa7f3e18ff"
                        }
                    ],
                    "text": "Оставьте Ваш номер телефона на следующем шаге и с вами свяжется Администратор",
                    "id": "66e7cac6-165b-429d-bcea-cc79e004f97d",
                    "type": "final_form",
                    "display": {
                        "union": "and",
                        "conditions": [
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "540c86d2-e21a-4323-b5c8-0a032719b25e",
                                "answer": [
                                    "bc8d632c-5c98-478f-893e-39d60d96a1db"
                                ]
                            },
                            {
                                "union": "and",
                                "unionExtra": true,
                                "question": "cbb48b57-8011-4d3c-bad1-2ab6f0d0eeb0",
                                "answer": [
                                    "03438fb2-728e-41fa-8f63-8fc47d5ba6ca"
                                ]
                            }
                        ]
                    }
                }
            ],
        },
        userAnswers: {
            name: '',
            phone: '',
            answers: []
        },
        referalUtm: {
            utm_source: '',
            utm_medium: '',
            utm_campaign: '',
            utm_term: ''
        },
        showModal: false
    },
    actions: {
        sendQuizResult(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            return axios.post('/wp-admin/admin-ajax.php', params, config)
                .then(response => {
                    // context.commit('errorsFormData', {});
                    console.log(response);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        getQuizData(context, params) {
            const config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            let form_data = new FormData();
            form_data.append('action', params.action);
            form_data.append('smartquiz_action', params.smartquiz_action);
            form_data.append('quiz_id', params.quiz_id);

            return axios.post("/wp-admin/admin-ajax.php", form_data, config)
                .then(response => {
                    context.commit('setQuizData', response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        }

    },
    mutations: {
        changeQuizData(state) {
            this.$store.state.quizData = state;
        },
        setShowModal(state, data) {
            state.showModal = data;
        },
        setReferalUtm(state, data) {
            state.referalUtm = data;
        },
        resetAnswers(state, data) {
            state.quizData.questions.forEach(function (item) {
                item.userAnswer = null;
            })
        },
        setUserAnswer(state, data) {
            const objIndex = state.quizData.questions.findIndex((obj => obj.id === data.question_id));

            if (objIndex !== -1) {
                state.quizData.questions[objIndex].userAnswer = data.answer.id;
            }
        },

        changeUserAnswers(state, data) {
            let isPush = true;
            if (state.userAnswers.answers.length > 0) {
                const objIndex = state.userAnswers.answers.findIndex((obj => obj.question_id === data.question_id));
                if (objIndex !== -1) {
                    state.userAnswers.answers[objIndex].answer = data.answer;
                    isPush = false;
                }
            }
            if (isPush === true) {
                state.userAnswers.answers.push(data);
            }
        },
        setQuizData(state, data) {
            state.quizData = data;
        }
    }
})