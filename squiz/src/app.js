import Vue from 'vue'
import App from './components/App'
import Modal from './components/Modal'
import store from './store/index'
import VueSweetalert2 from 'vue-sweetalert2';
// import 'bootstrap/dist/css/bootstrap.css'

// export const eventEmitter = new Vue();
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.use(VueSweetalert2);

window.onload = function () {
    Vue.component('app-component', App);
    Vue.component('modal', Modal);

    new Vue({
        el: '#squiz-app',
        store
    })

};

